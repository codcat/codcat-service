import pytest


@pytest.mark.asyncio
async def test_predict(app, model):
    request, response = await app.asgi_client.post(
        app.url_for("api.predict"),
        json={"texts": ["def foo(bar): return bar"]},
    )

    assert response.json == model.predict_proba()
