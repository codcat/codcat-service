import logging
import pathlib
from unittest.mock import (
    MagicMock,
    patch,
)

import pytest
import yaml
from sanic_testing import TestManager

from codcat_service.main import create_app


@pytest.fixture()
def model():
    model = MagicMock()
    model.predict.return_value = ["python"]
    model.predict_proba.return_value = [
        {"python": 0.9, "java": 0.1, "c++": 0.0}
    ]
    return model


@pytest.fixture
def app(model):
    def setup(sa):
        sa.ctx.codcat_model = model

    with patch("codcat_service.main.setup_model", new=setup):
        sanic_app = create_app()
    TestManager(sanic_app)
    yield sanic_app


@pytest.fixture
def logger():
    logger = logging.getLogger(__name__)
    numeric_level = getattr(logging, "DEBUG", None)
    logger.setLevel(numeric_level)
    return logger
