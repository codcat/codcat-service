# Codcat Service - Code Language Classifier Web Service

This web service is designed to classify programming code snippets by their respective programming languages. The service uses an ONNX format model trained on a Python package to perform the classification.
The web service is built using the [Codcat](https://gitlab.com/codcat/codcat), Sanic framework in Python, and the frontend is built using Node.js and Tailwind CSS.

# Installation
To install the required dependencies for the web service, please run the following command:

```bash
pip install -r requirements.txt
```

To install the required dependencies for the frontend, please run the following command:

```bash
cd codcat_service/static
npm install
```

To start the web service, simply run the following command:

```bash
python main.py
```

This will start the web service on http://localhost:8000. You can then send a POST request to http://localhost:8000/api/v1/predict with the code snippet you want to classify in the request body as a JSON object with the key code. For example:

```json
{
  "texts": ["def hello_world():\n    print('Hello, World!')\n\nhello_world()"]
}
```

You can open docs on http://localhost:8000/docs to see the API documentation.

To start the frontend, run the following command:

```bash
cd codcat_service/static
npx tailwindcss -i ./input.css -o ./output.css --watch
```

# Models
The web service uses an ONNX format model trained on a Python package to perform the classification. The model files should be placed in the models directory. The models should be named model.onnx and should be compatible with ONNX Runtime.

# License
This project is licensed under the MIT License - see the LICENSE file for details.
