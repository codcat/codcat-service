"""
This module provides a Jinja environment for rendering templates in the Sanic web framework.

The Jinja environment is configured to load templates from two locations:
- the "static" directory in the root of the project directory
- the "node_modules" directory inside the "static" directory

It also provides a `JinjaRenderer` class that allows rendering templates
and updating the context with Sanic request information.
"""

from pathlib import Path

import jinja2
from jinja2 import (
    ChoiceLoader,
    Environment,
    FileSystemLoader,
)
from sanic import html


class JinjaRenderer:
    """A renderer for Jinja templates."""

    def __init__(self):
        """Initialize the Jinja environment with multiple loaders."""
        static = Path(__file__).parent.parent.joinpath("static")
        loader1 = FileSystemLoader(static)
        loader2 = FileSystemLoader(static.joinpath("node_modules"))
        loader = ChoiceLoader([loader1, loader2])
        self.env = Environment(loader=loader, autoescape=True)

    @staticmethod
    def update_request_context(request, context):
        """
        Update the context dictionary with the Sanic request object and the URL
        generator function.
        """
        context.setdefault("request", request)
        context.update({"url_for": request.app.url_for})

    def render_string(self, template_name, request, **context):
        """
        Render a Jinja template to a string using the provided context.
        """
        self.update_request_context(request, context)
        return self.env.get_template(template_name).render(**context)

    def render(self, template_name, request, **context):
        """
        Render a Jinja template to a Sanic HTML response using the provided context.
        """
        return html(self.render_string(template_name, request, **context))


jinja2.select_autoescape(
    enabled_extensions=("html", "htm", "xml"),
    disabled_extensions=(),
    default_for_string=True,
    default=False,
)


def setup_jinja(app):
    """Set up the Jinja renderer and attach it to the Sanic app context."""
    app.ctx.jinja = JinjaRenderer()
