"""Main module for the Codcat service."""

import logging
import sys
from importlib import import_module
from pathlib import Path

from rich.traceback import install
from sanic import Sanic
from sanic.worker.loader import AppLoader

from codcat_service.domain.model_setup import setup_model
from codcat_service.tools.j2 import setup_jinja

log = logging.getLogger("codcat-service")

modules_names = ("codcat_service.blueprints.api",)


def create_app():
    # Create the Sanic app
    app = Sanic("codcat-service")

    # Setup Jinja templates
    setup_jinja(app)

    @app.route("/", methods=["GET", "POST"])
    async def root(request):
        """
        The root route for the Codcat service.

        Returns:
            The rendered "index.html" Jinja template.
        """
        return request.app.ctx.jinja.render("index.html", request)

    # Install Rich traceback
    install()

    # Serve static files
    app.static("/static", Path(__file__).parent.joinpath("static"))

    # Register blueprints from the modules
    for module_name in modules_names:
        module = import_module(module_name)
        sys.modules[module.__name__] = module
        bp = getattr(module, "bp", None)
        if bp:
            print(f"Registering blueprint `{bp.name}`")
            app.blueprint(bp)

    setup_model(app)

    return app


if __name__ == "__main__":
    loader = AppLoader(factory=create_app)
    sanic_app = loader.load()
    sanic_app.prepare(port=8000, dev=False, auto_reload=True, workers=1)
    Sanic.serve(primary=sanic_app, app_loader=loader)
