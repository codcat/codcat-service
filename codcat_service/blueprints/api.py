"""Module containing the blueprint for the Codcat API."""

from sanic import (
    Blueprint,
    json as sanic_json,
)
from sanic_ext import (
    openapi as doc,
    validate,
)

from codcat_service.models.predict_request import PredictRequest

bp = Blueprint("api", version_prefix="/api/v", version="1")


# Define a route for the '/predict' endpoint
@bp.route("/predict", methods=["POST"])
# Define the OpenAPI documentation for the endpoint
@doc.definition(
    body={"application/json": PredictRequest.schema()},
    summary="Predicts the probability of each class for a given text.",
)
# Use the 'validate' decorator to validate the request JSON against the 'PredictRequest' schema
# and store the validated request in the 'predict_request' variable
@validate(json=PredictRequest, body_argument="predict_request")
async def predict(request, predict_request: PredictRequest):
    """
    Endpoint for predicting the probability of each class for a given text.

    Args:
        request: The Sanic request object.
        predict_request: The validated PredictRequest object parsed from the request body.

    Returns:
        A JSON response containing the predicted probabilities for each class.
    """
    # Extract the text from the validated 'PredictRequest' object
    texts = predict_request.texts
    # Get the Codcat model from the Sanic app context
    codcat_model = request.app.ctx.codcat_model
    # Use the model to predict the probabilities for each class given the text
    predicted_probabilities = codcat_model.predict_proba(texts)
    # Return the predicted probabilities as a JSON response
    return sanic_json(predicted_probabilities)
