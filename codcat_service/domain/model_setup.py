from codcat.downloader import load


def setup_model(application):
    application.ctx.codcat_model = load("base-tiny")
    return application
