from typing import List

from pydantic import BaseModel


class PredictRequest(BaseModel):
    """
    Pydantic model representing a request to predict the probability of each class for a given text.

    Attributes:
        texts (List[str]): The list of texts to predict the class probabilities for.
    """

    texts: List[str]
